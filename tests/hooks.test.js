const test = require('node:test')
const assert = require('node:assert').strict
const { resolve } = require('node:path')
const { mkdtemp } = require('node:fs/promises')
const { tmpdir } = require('node:os')

const { collectHooks, runHook } = require('../hooks')

test('collect hooks', async (t) => {
  t.after(() => {
    delete process.env.ST_HOOK_ROOT
  })

  process.env.ST_HOOK_ROOT = resolve(__dirname, '__fixtures__', 'hello')
  const hooks = await collectHooks()
  assert.equal(hooks.length, 1)
})

test('run hello hook', async (t) => {
  t.after(() => {
    delete process.env.ST_HOOK_ROOT
  })
  process.env.ST_HOOK_ROOT = resolve(__dirname, '__fixtures__', 'hello')
  const hooks = await collectHooks()
  await assert.doesNotReject(runHook(hooks[0]))
})

test('failing hook rejects', async (t) => {
  t.after(() => {
    delete process.env.ST_HOOK_ROOT
  })
  process.env.ST_HOOK_ROOT = resolve(__dirname, '__fixtures__', 'error')
  const hooks = await collectHooks()
  await assert.rejects(runHook(hooks[0]))
})

test('no hooks to run when no hooks are found', async (t) => {
  t.after(() => {
    delete process.env.ST_HOOK_ROOT
  })

  {
    process.env.ST_HOOK_ROOT = await mkdtemp(resolve(tmpdir(), 'st-hook-'))
    const hooks = await collectHooks()
    assert.equal(hooks.length, 0)
  }

  {
    process.env.ST_HOOK_ROOT = resolve(__dirname, '__fixtures__', 'doesntexist')
    const hooks = await collectHooks()
    assert.equal(hooks.length, 0)
  }
})
