const test = require('node:test')
const assert = require('node:assert').strict
const { getEnvVar } = require('../env')

test('get value from process.env', async (t) => {
  t.after(() => {
    delete process.env.ST_HOOKS_TEST
  })

  process.env.ST_HOOKS_TEST = 'test'
  const value = getEnvVar('ST_HOOKS_TEST')
  assert.equal(value, 'test')
})

test('get default value when process.env is not set', async (t) => {
  const value = getEnvVar('ST_HOOKS_TEST', 'default')
  assert.equal(value, 'default')
})

test('return undefined when process.env is not set and no default value is provided', async (t) => {
  const value = getEnvVar('ST_HOOKS_TEST')
  assert.equal(value, undefined)
})
