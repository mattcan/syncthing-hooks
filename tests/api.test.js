const test = require('node:test')
const assert = require('node:assert').strict
const nock = require('nock')

const { fetchNewEvents } = require('../api')

process.env.ST_URL = 'http://localhost:8888'
process.env.ST_API_KEY = '1234567890'

test('fetchNewEvents', async () => {
  const events = [
    {
      id: '123',
      data: {
        foo: 'bar'
      }
    },
    {
      id: '456',
      data: {
        foo: 'baz'
      }
    }
  ]

  nock(process.env.ST_URL)
    .get('/rest/events')
    .reply(200, events)

  const { events: newEvents, seenIds } = await fetchNewEvents(new Set())

  assert.deepEqual(newEvents, events)
  assert.deepEqual(seenIds, new Set(['123', '456']))
})

test('fetchNewEvents with seenIds', async () => {
  const events = [
    {
      id: '123',
      data: {
        foo: 'bar'
      }
    },
    {
      id: '456',
      data: {
        foo: 'baz'
      }
    }
  ]

  nock(process.env.ST_URL)
    .get('/rest/events')
    .reply(200, events)

  const { events: newEvents, seenIds } = await fetchNewEvents(new Set(['123']))

  assert.deepEqual(newEvents, [events[1]])
  assert.deepEqual(seenIds, new Set(['123', '456']))
})
