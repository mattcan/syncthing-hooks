module.exports = {
  getEnvVar: function (varname, defaultvalue) {
    const result = process.env[varname]
    if (result !== undefined) { return result } else { return defaultvalue }
  }
}
